package com.gensoukyou1337.evenfibonacci;

import java.util.List;
import java.util.ArrayList;

public class Main
{
	static List<Integer> fibArray = new ArrayList<Integer>(){{
		add(0);
		add(1);
	}}; // Initializes the ArrayList with initial 2 elements of the fibonacci sequence
	
	public static void main(String[] args)
	{
		int result = 0; // Initializes the result variable to append to the ArrayList
		int sum = 0; // Initializes the sum to add all the even numbers
		while(result < 4000000)
		{
			int firstNum = fibArray.get(fibArray.size() - 1); // Gets the last element in the ArrayList
			int secondNum = fibArray.get(fibArray.size() - 2); // Gets the second last element in the ArrayList
			result = firstNum + secondNum; // Adds them together [like in every fibonacci sequence, a number is the sum of the 2 numbers before it]
			if(result % 2 == 0) // Check if the number is even
			{
				sum += result; // Adds the number to the variable 'sum' if so
			}
			fibArray.add(result); // Appends the ArrayList with the result of the addition
		}
		System.out.println(sum); // Outputs sum of even numbers in the fibonacci sequence below 4 million
	}
}
